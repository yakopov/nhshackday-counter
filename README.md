# What is this?

A working prototype of the counter app developed on [NHS Hack Day](http://nhshackday.com) 2016 in Newcastle.

Here is the initial [pitch](https://docs.google.com/document/d/10TEFpyeb3OKfAqllWunq9OlKNVQ3EgVrHRW3JjjZApA/edit?usp=sharing) of the idea.

Here is the final [presentation](https://docs.google.com/presentation/d/1BZPQRR3c8-JwROIsUYzd8JR9RzJCx3D1vcBcGtuU4ow/edit?usp=sharing).

# What it does?

It allows to count events of different types separately including of types not defined before, which is achieved by
offering random types in addition to user-defined ones.

Such random types, if they were used, can be reviewed later when the time permits and assigned relevant name, icon etc.

The general idea of the app is that:

* You always have a option to record an event separately from previous counts quickly if anything unexpected happens
* Even though the type icon is random for such sudden events, it is still a memory marker that makes memorising the
context and relevant details easier than when then looking at just an abstract number.

# How do I run it?

* Make sure you have Python 3 installed.
* Clone repository.
* Install dependencies: `pip --install -r requirements.txt` (make sure that pip uses Python3: `pip --version`)
* Edit `config.json` to change the WSGI app host and port number and other properties, if required.
* run `./app.py --env development` (or use another name of the environment you defined in `config.json`).
* Visit that port and host in your browser. Here are the implemented pages:
    * `/` - counter screen
    * `/types` - list of types
    * `/events` - list of events
    * `/graphs` - graphs and charts

# Is it fully ready?

No. It is a proof of concept only. Even a simple app like this requires surprisingly a lot of boilerplate code so I cut
a lot of corners here. For example, there are no users and separation of their data, no pagination and so on.

Some task-specific features are also missing. There is no merging or splitting of types, and type hierarchy is supported
on the data level, but is not used in the logic.

Finally, in order to be usable in real life for the intended purpose, it needs a native mobile app front end that can
work offline and sync later.

Existing code needs refactoring as well as it was put up in a hurry to meet hackday deadlines, especially the JavaScript
part of it.

It still runs and allows to see whether the idea is working (i.e. if it is indeed convenient to count events like this).

# I have a question

Please write me an [email](mailto:akopov@hotmail.co.uk)