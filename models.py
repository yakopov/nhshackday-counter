from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, ForeignKey
from sqlalchemy.types import String, Integer, Text, DateTime, Float, Boolean
from sqlalchemy.orm import relationship
from datetime import datetime
from uuid import uuid4

"""
A subset of codes from here as that emoji font is used: http://emojisymbols.com/emojilist.php

@todo: lists below should be loaded from DB or config
"""
icons = [
    '&#x1F311;',    # black circle
    '&#x1F317;',    # half-black circle,
    '&#x1F320;',    # comet
    '&#x1F338;',    # flower
    '&#x1F49B;',    # white hard
    '&#x1F49C;',    # black heart
    '&#x1F55D;',    # clock
    '&#x1F6A5;',    # horizontal traffic lights
    '&#x1F6A6;',    # vertical traffic lights
    '&#x25FC;',     # black square
    '&#x25FC;',     # white square
    '&#x2734;',     # black star
    '&#x2733;',     # white star
    '&#x2795;',     # black plus
    '&#x1F308;',    # rainbow
]

icon_colours = [
    '#aa0000',
    '#00aa00',
    '#0000aa',
    '#000000',
    '#aaaa00',
    '#aa00aa',
    '#00aaaa'
]


def new_uuid():
    return str(uuid4())


Base = declarative_base()


class EventType(Base):
    __tablename__ = 'event_types'

    uuid = Column(String, default=new_uuid, primary_key=True)
    parent_uuid = Column(String, ForeignKey(uuid))

    name = Column(String)
    reviewed = Column(Boolean, default=False)   # false if the type has been created automatically

    icon = Column(String)
    icon_colour = Column(String)

    events = relationship('Event', back_populates='type')
    parent = relationship('EventType')

    def to_json(self):
        return {
            '_model': 'EventType',

            'uuid': self.uuid,
            'parent_uuid': self.parent_uuid,

            'name': self.name,
            'reviewed': self.reviewed,

            'icon': self.icon,
            'icon_colour': self.icon_colour,

            'event_count': len(self.events)
        }


class Event(Base):
    __tablename__ = 'events'

    id = Column(Integer, primary_key=True)
    type_uuid = Column(String, ForeignKey(EventType.uuid))

    date = Column(DateTime, default=datetime.now)

    latitude = Column(Float(precision=64))
    longitude = Column(Float(precision=64))

    comment = Column(Text)

    type = relationship('EventType', back_populates='events')

    def to_json(self):
        return {
            '_model': 'Event',

            'id': self.id,
            'type_uuid': self.type_uuid,

            'date': self.date.strftime("%Y-%m-%d %H:%M:%S"),

            'latitude': self.latitude,
            'longitude': self.longitude,

            'comment': self.comment,

            'type': self.type.to_json()
        }
