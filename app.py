#!/usr/local/bin/python3.5
import cherrypy
import os
from cp_sqlalchemy import SQLAlchemyTool, SQLAlchemyPlugin

import models
import api
import pages
import config

from pprint import pprint

APP_PATH = os.path.abspath(os.curdir)


cherrypy.config.update({
    'environment': 'production',    # config.env,
    'log.screen': True,
    'server.socket_host': config.config_val('webapp', 'host'),
    'server.socket_port': config.config_val('webapp', 'port')
})


def run():
    cherrypy.tools.db = SQLAlchemyTool()

    cherrypy.tree.mount(
        pages.Root(), '/',
        {
            '/': {
                'tools.db.on': True
            },
            '/static': {
                'tools.staticdir.on': True,
                'tools.staticdir.dir': os.path.join(APP_PATH, 'static')
            }
        }
    )
    cherrypy.tree.mount(
        api.EventRest(), '/api/event',
        {
            '/': {
                'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                'tools.db.on': True
            },

        }
    )
    cherrypy.tree.mount(
        api.EventTypeRest(), '/api/type',
        {
            '/': {
                'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                'tools.db.on': True
            }
        }
    )

    db_path = os.path.join(APP_PATH, 'data', config.config_val('connections', 'db'))

    if not os.path.exists(db_path):
        open(db_path, 'w+').close()

    sqlalchemy_plugin = SQLAlchemyPlugin(
        cherrypy.engine, models.Base, 'sqlite:///%s' % db_path,
        echo=True
    )
    sqlalchemy_plugin.subscribe()
    sqlalchemy_plugin.create()

    cherrypy.engine.start()
    cherrypy.engine.block()

if __name__ == '__main__':
    run()
