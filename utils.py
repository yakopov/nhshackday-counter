import unittest

"""
A place for generic purpose functions without a business context
"""


def uniquify(seq, id_fun=None):
    """
    Removes duplicates from the given structure
    """

    if id_fun is None:
        def id_fun(x):
            return x

    seen = {}
    result = []

    for item in seq:
        marker = id_fun(item)
        if marker in seen:
            continue

        seen[marker] = True
        result.append(item)

    return result


def dict_val(d, *keys, default=None):
    """
    Allows to address a dictionary through a sequence of keys
    """

    value = d
    for key in keys:
        try:
            value = value[key]
        except (KeyError, IndexError):
            if default is not None:
                return default
            else:
                raise KeyError("No config key found for the path [" + "][".join(
                    str(x) for x in keys
                ) + "]")

    return value


def diff_values(a, b):
    """
    Subtracts sequence b from a, i.e. returns values from a which are not found in b
    """
    b = set(b)
    return [aa for aa in a if aa not in b]


def extend_dict(own, inherit_from):
    """
    Extends given dictionary with another one respecting sub-dictionaries
    """

    if not(isinstance(own, dict) and isinstance(inherit_from, dict)):
        raise TypeError(__name__ + " is only applicable to dictionaries (" +
                        type(own).__name__ + " and " + type(inherit_from).__name__ + " supplied")

    inherited = inherit_from.copy()

    for (key, value) in own.copy().items():
        if key in inherit_from:             # if there is a conflict
            # we only need to go deeper if both conflicting keys are dictionaries
            # otherwise own value overrides the inherited one
            # if they are both dictionaries though, we need to go key by key
            if isinstance(inherited[key], dict) and isinstance(own[key], dict):
                inherited[key] = extend_dict(own[key], inherit_from[key])
                continue

        inherited[key] = own[key]

    return inherited


def copy_dict_part(src, fields):
    res = {}
    for field in fields:
        res[field] = src[field]

    return res


def lists_equal(a, b):
    if len(a) != len(b):
        return False

    for item in a:
        if item not in b:
            return False

    for item in b:
        if item not in a:
            return False

    return True


class UtilsTest(unittest.TestCase):
    def test_diff_values(self):
        a = [1, 2, 3, 4]
        b = [2, 5]

        res = diff_values(a, b)
        self.assertEqual(len(res), 3)
        self.assertEqual(res[0], 1)
        self.assertEqual(res[1], 3)
        self.assertEqual(res[2], 4)

        res = diff_values(b, a)
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0], 5)

    def test_copy_dict_part(self):
        dest = {
            'foo': 123,
            'bar': 345
        }

        src = {
            'foo': 111,
            'bar': 101
        }

        dest.update(copy_dict_part(src, ['foo']))
        self.assertEqual(dest['foo'], 111)
        self.assertEqual(dest['bar'], 345)

    def test_uniquify(self):
        s = [0, 1, 2, 2, 1, 0, 4, 3, 4, 5, 1]

        scenarios = [
            {
                'su': uniquify(s),
                'expected': [0, 1, 2, 4, 3, 5]
            },
            {
                'su': uniquify(s, lambda x: bool(x)),
                'expected': [False, True]
            },
        ]

        for sc in scenarios:
            su = sc['su']
            expected = sc['expected']

            for (index, item) in enumerate(expected):
                self.assertEqual(su[index], item)

            self.assertEqual(len(su), len(expected))

    def test_dict_val(self):
        d = {
            'foo': {
                'bar': 90
            },
            'foobar': [
                10,
                20
            ]
        }

        self.assertEqual(dict_val(d, 'foo', 'bar'), 90)
        self.assertEqual(dict_val(d, *['foo', 'bar']), 90)
        self.assertEqual(dict_val(d, 'foo')['bar'], 90)
        self.assertEqual(dict_val(d, 'foo', 'foo', default=123), 123)

        self.assertRaises(KeyError, dict_val, d, 'foo', 1)
        self.assertRaises(KeyError, dict_val, d, 'foo', 'foo')

        self.assertEqual(dict_val(d, 'foobar', 1), 20)
        self.assertRaises(KeyError, dict_val, d, 'foobar', 3)

    def test_extend_dict(self):
        own = {
            'connection': {
                'server': "foo.bar",
                'login': "development"
            },

            'nested_scalar_1': 123,
            'nested_scalar_2': {
                'nested_key': "child nested value"
            },

            'param_1': "child param_1",
            'param_3': "child param_3"
        }

        inherit_from = {
            'param_1': "parent param_1",
            'param_2': "parent param_2",
            'param_4': "parent param_4",

            'connection': {
                'server': "foo.bar",
                'port': 123,
                'login': "production"
            },

            'nested_scalar_1': {
                'nested_key': "parent nested value"
            },
            'nested_scalar_2': 345
        }

        merged = extend_dict(own, inherit_from)

        self.assertEqual(merged['connection']['server'], "foo.bar")
        self.assertEqual(merged['connection']['port'], 123)
        self.assertEqual(merged['connection']['login'], "development")

        self.assertEqual(merged['nested_scalar_1'], 123)
        self.assertEqual(merged['nested_scalar_2']['nested_key'], "child nested value")

        self.assertEqual(merged['param_1'], "child param_1")
        self.assertEqual(merged['param_2'], "parent param_2")
        self.assertEqual(merged['param_3'], "child param_3")
        self.assertEqual(merged['param_4'], "parent param_4")


if __name__ == '__main__':
    unittest.main()
