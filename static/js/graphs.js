function renderCalendarView() {
    var myConfig = {
        "graphset":[
            {
                "type":"calendar",
                "id":"G1",
                "plotarea":{
                  "margin":"200 50 200 90"
                },
                "options":{
                    "year":{
                        "text": 2016
                    },
                    "scale": {},
                    "rows": 1,
                    "values": calendarValues
                },
                "tooltip": {}
            }
        ]
    };

    zingchart.render({
        id : 'calendar-view',
        data : myConfig,
        height: 500,
        width: 725
    });
}

function renderChartView() {
    var myConfig = {
        type: "pie",
        backgroundColor: "#ffffff",
        plot: {
          borderColor: "#2B313B",
          borderWidth: 5,
          // slice: 90,
          valueBox: {
            placement: 'out',
            text: '%t\n%npv%',
            fontFamily: "Open Sans"
          },
          tooltip:{
            fontSize: '18',
            fontFamily: "Open Sans",
            padding: "5 10",
            text: "%npv%"
          },
          animation:{
          effect: 2,
          method: 5,
          speed: 500,
          sequence: 1
        }
        },
        source: {
          text: 'gs.statcounter.com',
          fontColor: "#8e99a9",
          fontFamily: "Open Sans"
        },
        title: {
          fontColor: "#fff",
          text: 'Events by group',
          align: "left",
          offsetX: 10,
          fontFamily: "Open Sans",
          fontSize: 25
        },
        subtitle: {
          offsetX: 10,
          offsetY: 10,
          fontColor: "#8e99a9",
          fontFamily: "Open Sans",
          fontSize: "16",
          text: 'May 2016',
          align: "left"
        },
        plotarea: {
          margin: "20 0 0 0"
        },
        series : chartValues
    };

    zingchart.render({
        id : 'chart-view',
        data : myConfig,
        height: 500,
        width: 725
    });
}

$(document).ready(function() {
    renderCalendarView();
    renderChartView();
});
