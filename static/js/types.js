function loadTypes() {
    $.ajax({
        method: 'GET',
        contentType: 'application/x-www-form-urlencoded',
        url: serverRoot + '/api/type',
        data: {}
    }).done(function(response) {
        console.log(response);

        $.each(response, function(index, json) {
            typeDivFromJson(json);
        });
    })
}

function populateTypeFromJson(form, json) {
    form.find('input[name=type_uuid]').val(json.uuid);
    form.find('select[name=parent_uuid]').val(json.parent_uuid);

    form.find('input[name=name]').val(json.name);
    form.find('input[name=reviewed]').val(json.reviewed ? 1 : 0);

    if (!json.reviewed) {
        form.find('label').addClass('not-reviewed');
    } else {
        form.find('label').removeClass('not-reviewed');
    }

    form.find('input[name=icon]').val(json.icon);
    form.find('input[name=icon_colour]').val(json.icon_colour);

    if (json.icon && json.icon.length) {
        form.find('span.emj').html(json.icon);
        if (json.icon_colour && json.icon_colour.length) {
            form.find('span.emj').css('color', json.icon_colour)
        }
    }

    form.find('.view-events').attr('href', serverRoot + '/events?type_uuid=' + json.uuid);
}

function typeDivFromJson(json) {
    var template = $('div.new-type-template .type-form').clone(true, true);

    populateTypeFromJson(template, json);

    $('div.type-list').append(template);
}

function getFormData(element) {
    var form = $(element).closest('.type-form');

    var values = {};
    $.each(form.serializeArray(), function(i, field) {
        values[field.name] = field.value;
    });

    console.log("Form data:");
    console.log(values);

    return values;
}

$(document).ready(function() {
    $('.create-type').click(function() {
        console.log("Creating a type...");

        var values = getFormData(this);

        $.ajax({
            method: 'POST',
            url: serverRoot + '/api/type/',
            data: values

        }).done(function(response) {
            console.log("Type created");
            console.log(response);

            typeDivFromJson(response);
        });
    });

    $('.update-type').click(function() {
        console.log("Updating a type...");

        var values = getFormData(this);
        var button = $(this);

        $.ajax({
            method: 'PUT',
            url: serverRoot + '/api/type/',
            data: values,

        }).done(function(response) {
            console.log("Type updated");
            console.log(response);

            populateTypeFromJson(button.closest('.type-form'), response);
        });
    });

    $('.delete-type').click(function() {
        console.log("Deleting a type...");

        var values = getFormData(this);
        var button = $(this);

        $.ajax({
            method: 'DELETE',
            url: serverRoot + '/api/type/?type_uuid=' + values['type_uuid']

        }).done(function(response) {
            console.log("Type deleted");
            console.log(response);

            button.parent().closest('.type-container').remove();
        });
    });

    loadTypes();
});