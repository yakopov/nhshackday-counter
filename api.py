import cherrypy
import models
from sqlalchemy import desc
from urllib.parse import urlparse
import random
from pprint import pprint


def get_non_empty_param(param, default=None):
    """Converts empty parameters to Nones"""
    if len(str(param)) == 0:
        return default

    return param


class Rest(object):
    exposed = True

    @property
    def db(self):
        return cherrypy.request.db

    @property
    def mount_point(self):
        parsed = urlparse(cherrypy.url())
        path = parsed.path

        if not(path.endswith('/')):
            return path + '/'

        return path


class EventTypeRest(Rest):
    def get_type(self, type_uuid, ):
        return self.db.query(models.EventType).filter_by(uuid=type_uuid).first()

    @cherrypy.tools.json_out()
    def GET(self, type_uuid=None):
        type_uuid = get_non_empty_param(type_uuid)

        if type_uuid is None:
            types = self.db.query(models.EventType).order_by(models.EventType.name).all()
            response = []
            for type in types:
                response.append(type.to_json())

            return response

        elif type_uuid == 'random':
            random_type = models.EventType(uuid=models.new_uuid(), name="A new thing",
                                           icon=random.choice(models.icons),
                                           icon_colour=random.choice(models.icon_colours))
            return random_type.to_json()

        else:
            event_type = self.get_type(type_uuid)
            return event_type.to_json()

    def POST(self, name=None, parent_uuid=None, icon=None, icon_colour=None, reviewed=False):
        pprint('Entered type post method')

        name = get_non_empty_param(name)
        parent_uuid = get_non_empty_param(parent_uuid)
        icon = get_non_empty_param(icon)
        icon_colour = get_non_empty_param(icon_colour)
        reviewed = bool(reviewed)

        new_type = models.EventType(uuid=models.new_uuid(), name=name, parent_uuid=parent_uuid,
                                    icon=icon, icon_colour=icon_colour, reviewed=reviewed)
        self.db.add(new_type)
        self.db.commit()

        raise cherrypy.HTTPRedirect(self.mount_point + '?type_uuid=' + new_type.uuid)

    def PUT(self, type_uuid, parent_uuid=None, name=None, icon=None, icon_colour=None, reviewed=False):
        parent_uuid = get_non_empty_param(parent_uuid)
        name = get_non_empty_param(name)
        icon = get_non_empty_param(icon)
        icon_colour = get_non_empty_param(icon_colour)
        reviewed = bool(reviewed)

        event_type = self.get_type(type_uuid)
        event_type.parent_uuid = parent_uuid
        event_type.name = name
        event_type.reviewed = reviewed
        event_type.icon = icon
        event_type.icon_colour = icon_colour

        self.db.add(event_type)
        self.db.commit()

        raise cherrypy.HTTPRedirect(self.mount_point + '?type_uuid=' + event_type.uuid)

    @cherrypy.tools.json_out()
    def DELETE(self, type_uuid):
        type = self.get_type(type_uuid)

        for event in type.events:
            self.db.delete(event)

        self.db.delete(type)
        self.db.commit()

        return "ok"


class EventRest(Rest):
    def get_event(self, event_id=None, type_uuid=None):
        if event_id is None:
            query = self.db.query(models.Event).order_by(desc(models.Event.date))

            if type_uuid is not None:
                query.filter_by(type_uuid=type_uuid)

            events = query.all
            response = []

            for event in events:
                response.append(event.to_json())

            return response

        else:
            return self.db.query(models.Event).filter_by(id=event_id).first()

    @cherrypy.tools.json_out()
    def GET(self, event_id):
        event = self.get_event(event_id)
        return event.to_json()

    def POST(self, type_uuid=None, name=None, icon=None, icon_colour=None, latitude=None, longitude=None):
        type_uuid = get_non_empty_param(type_uuid, models.new_uuid())
        name = get_non_empty_param(name)
        icon = get_non_empty_param(icon)
        icon_colour = get_non_empty_param(icon_colour)

        latitude = get_non_empty_param(latitude)
        longitude = get_non_empty_param(longitude)

        # check if type with the supplied uuid does not exist yet and create it if it doesn't
        existing_type = self.db.query(models.EventType).filter_by(uuid=type_uuid).first()
        if existing_type is None:
            new_type = models.EventType(uuid=type_uuid, name=name, icon=icon, icon_colour=icon_colour, reviewed=False)
            self.db.add(new_type)

        new_event = models.Event(type_uuid=type_uuid, latitude=latitude, longitude=longitude)
        self.db.add(new_event)
        self.db.commit()

        # here and everywhere in similar cases URL must be figured out
        raise cherrypy.HTTPRedirect(self.mount_point + '?event_id=' + str(new_event.id))

    def PUT(self, event_id, type_uuid=None, comment=None):
        event = self.get_event(event_id)

        comment = get_non_empty_param(comment)
        event.comment = comment

        type_uuid = get_non_empty_param(type_uuid)
        if len(type_uuid) > 0:
            event.type_uuid = type_uuid

        self.db.add(event)
        self.db.commit()

        raise cherrypy.HTTPRedirect(self.mount_point + '?event_id=' + str(event.id))

    @cherrypy.tools.json_out()
    def DELETE(self, event_id):
        event = self.get_event(event_id)
        self.db.delete(event)
        self.db.commit()

        return "ok"
