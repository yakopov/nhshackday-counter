import json
from utils import extend_dict, dict_val
from os import path
from argparse import ArgumentParser


def get_arg_parser():
    """
    Returns command line option parser

    :return: argparse.ArgumentParser
    """
    parser = ArgumentParser(description="ShipServ CLI application")
    parser.add_argument('--env', dest='env', required=False, default="development", help="environment to run in")

    return parser


def _extend_config(config):
    inherit_from_name = None
    try:
        inherit_from_name = dict_val(config, 'extends')
        inherit_from = _config_all['environments'][inherit_from_name]

        inherit_from = _extend_config(inherit_from)
        config = extend_dict(config, inherit_from)

    except KeyError:
        if inherit_from_name is None:
            pass    # this configuration is standalone and doesn't extend another one
        else:
            raise KeyError("Inherited environment " + inherit_from_name + " not found. Expected Values: " +
                           ", ".join(_config_all['environments'].keys()))

    return config

with open(path.join(path.dirname(__file__), 'config.json')) as f:
    _config_all = json.loads(f.read())   # whole config json

_parser = get_arg_parser()
_args = _parser.parse_known_args()[0]

env = _args.env
try:
    config_env = _extend_config(_config_all['environments'][env])   # config branch for requested mode
    version = _config_all['version']
except KeyError:
    raise KeyError("Environment " + env + " not specified or invalid. Expected Values: " +
                   ", ".join(_config_all['environments'].keys()))


def config_val(*keys_path):
    return dict_val(config_env, *keys_path)
