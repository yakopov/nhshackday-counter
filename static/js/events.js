function getFormData(element) {
    var form = $(element).closest('.event-form');

    var values = {};
    $.each(form.serializeArray(), function(i, field) {
        values[field.name] = field.value;
    });

    console.log("Form data:");
    console.log(values);

    return values;
}

$(document).ready(function() {
    $('.update-event').click(function() {
        var button = $(this);
        var form = button.closest('.event-form');

        console.log("Updating event...");

        var formData = getFormData(button);

        $.ajax({
            method: 'PUT',
            url: serverRoot + '/api/event',
            data: formData

        }).done(function(response) {
            console.log("Updated event id=" + response.id);
            form.find('input[name=comment]').val(response.comment);
        });
    });

    $('.delete-event').click(function() {
        var button = $(this);
        var form = button.closest('.event-form');

        console.log("Deleting event...");

        $.ajax({
            method: 'DELETE',
            url: serverRoot + '/api/event?event_id=' + form.find('input[name=event_id]').val()

        }).done(function(response) {
            console.log("Deleted event");
            form.remove();
        });
    });
});