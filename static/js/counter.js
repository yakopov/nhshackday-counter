function buttonDivFromJson(json) {
    var template = $('div.new-button-template .event-form').clone(true, true);

    populateButtonFromJson(template, json);

    $('div.event-list').append(template);
}

function populateButtonFromJson(form, json) {
    form.find('input[name=type_uuid]').val(json.uuid);
    form.find('input[name=name]').val(json.name);

    form.find('input[name=icon]').val(json.icon);
    form.find('input[name=icon_colour]').val(json.icon_colour);

    form.find('.type-name').html(json.name);

    form.find('.view-events').html(json.event_count);
    form.find('.view-events').attr('href', serverRoot + '/events?type_uuid=' + json.uuid);

    if (json.event_count > 0) {
        form.find('.view-events').removeClass('zero-counter');
    } else {
        form.find('.view-events').addClass('zero-counter');
    }


    if (json.icon && json.icon.length) {
        form.find('span.emj').html(json.icon);
        if (json.icon_colour && json.icon_colour.length) {
            form.find('span.emj').css('color', json.icon_colour)
        }
    }
}

function getFormData(element) {
    var form = $(element).closest('.event-form');

    var values = {};
    $.each(form.serializeArray(), function(i, field) {
        values[field.name] = field.value;
    });

    console.log("Form data:");
    console.log(values);

    return values;
}

function loadInstantTypeButtons() {
    for (var i = 0; i < 3; i++) {
        $.ajax({
            method: 'GET',
            url: serverRoot + '/api/type',
            data: {
                type_uuid: 'random'
            }
        }).done(function(response) {
            var template = $('.new-button-template .event-form').clone(true, true);
            populateButtonFromJson(template, response);
            $('.instant-event-list').append(template);
        });
    }
}

function loadExistingTypeButtons() {
    $.ajax({
        method: 'GET',
        url: serverRoot + '/api/type',
        data: {}
    }).done(function(response) {
        $.each(response, function(index, json) {
            var template = $('.new-button-template .event-form').clone(true, true);
            populateButtonFromJson(template, json);
            $('.existing-event-list').append(template);
        });
    });
}

$(document).ready(function() {
    $('.emj').click(function() {
        var button = $(this);
        var form = button.closest('.event-form');

        form.find('.emj').css('display', 'none');
        form.find('.emj').closest('td').append('<img class="loading" src="/static/img/loading.gif" width=94 height=94>');

        navigator.geolocation.getCurrentPosition(function(position) {
            console.log("Reading location...");

            form.find('input[name=latitude]').val(position.coords.latitude);
            form.find('input[name=longitude]').val(position.coords.longitude);

            console.log("Adding event...");

            var formData = getFormData(button);

            $.ajax({
                method: 'POST',
                url: serverRoot + '/api/event',
                data: formData

            }).done(function(response) {
                console.log("Added event id=" + response.id);

                form.find('.loading').remove();
                form.find('.emj').css('display', 'block');

                form.find('.view-events').removeClass('zero-counter');
                form.find('.view-events').html(parseInt(form.find('.view-events').html()) + 1);
            });
        });
    });

    loadInstantTypeButtons();
    loadExistingTypeButtons();
});