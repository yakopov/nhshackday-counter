import cherrypy
from mako.lookup import TemplateLookup
from sqlalchemy import desc
import models
import config
import json
from pprint import pprint

lookup = TemplateLookup(directories=['templates'])


def serve_template(template_name, **kwargs):
    server_root =\
        config.config_val('webapp', 'protocol') + '://' +\
        cherrypy.config['server.socket_host'] +\
        ':' + str(cherrypy.config['server.socket_port'])

    template = lookup.get_template(template_name)
    return template.render(**kwargs, server_root=server_root)


class Root(object):
    @property
    def db(self):
        return cherrypy.request.db

    @cherrypy.expose
    def index(self):
        return serve_template('counter.html')

    @cherrypy.expose
    def types(self):
        query = self.db.query(models.EventType).order_by(models.EventType.name)
        types = query.all()

        return serve_template('types.html', types=types)

    @cherrypy.expose
    def events(self, type_uuid=None):
        if type_uuid is not None:
            events = self.db.query(models.Event).filter_by(type_uuid=type_uuid).\
                order_by(desc(models.Event.date)).all()
        else:
            events = self.db.query(models.Event).order_by(desc(models.Event.date)).all()

        return serve_template('events.html', events=events, type_uuid=type_uuid,
                              api_key=config.config_val('connections', 'googleMapsApiKey'))

    @cherrypy.expose
    def graphs(self):
        events = self.db.query(models.Event).all()

        event_calendar = {}
        event_types = {}
        total_events = 0;
        for event in events:
            day = event.date.strftime('%Y-%m-%d')

            if day not in event_calendar:
                event_calendar[day] = 0

            event_calendar[day] += 1

            type_uuid = event.type_uuid
            if type_uuid not in event_types:
                event_types[type_uuid] = 0

            event_types[type_uuid] += 1
            total_events += 1

        calendar_values = []
        for day in event_calendar:
            calendar_values.append([day, event_calendar[day]])

        chart_values = []
        for type_uuid in event_types:
            type = self.db.query(models.EventType).filter_by(uuid=type_uuid).first()
            chart_values.append({
                'values': [event_types[type_uuid] / total_events],
                'text': type.name,
                'background-color': type.icon_colour
            });

        return serve_template('graphs.html', calendar_values=json.dumps(calendar_values),
                              chart_values=json.dumps(chart_values))
